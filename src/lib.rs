use unicode_segmentation::UnicodeSegmentation;

/// This trait is necessary to handle characters that take more than one byte.
/// It abstracts the notion of *bytes* to instead use *graphemes*, which provides a more friendly way to insert/remove
/// characters.
pub trait NonAscii {
    /// Gives the byte position for the character at the index.
    /// # Examples
    /// ```
    /// use non_ascii::NonAscii;
    ///
    /// let string = String::from("élégant");
    /// assert_eq!(string.byte_position(2), 3);
    /// ```
    ///
    /// # Panics
    /// If the provided position is greater than the length (in graphemes) of the string, the function will panic.
    fn byte_position(&self, pos: usize) -> usize;
    /// Gives the graph position for the given byte index.
    /// # Examples
    /// ```
    /// use non_ascii::NonAscii;
    ///
    /// let string = String::from("élégant");
    /// assert_eq!(string.graph_position(2), 1);
    /// ```
    ///
    /// # Panics
    /// If the provided position is not char boundary.
    fn graph_position(&self, pos: usize) -> usize;
    /// Returns the length of the string in *Unicode graphemes*.
    /// # Examples
    /// ```
    /// use non_ascii::NonAscii;
    ///
    /// let mut string = String::from("élégant");
    /// assert!(string.graph_len() < string.len());
    /// ```
    fn graph_len(&self) -> usize;
}

/// This trait is the mutable counterpart of the original trait/
pub trait NonAsciiMut {
    /// Insert character at the index.
    /// # Examples
    /// ```
    /// use non_ascii::NonAsciiMut;
    ///
    /// let mut string = String::from("élégant");
    /// // The following should panice
    /// // string.insert(1, 'c');
    /// string.safe_insert(1, 'c');
    /// assert_eq!(string.as_str(), "éclégant");
    /// ```
    /// # Panics
    /// If the provided position is greater than the length (in graphemes) of the string, the function will panic.
    fn safe_insert(&mut self, pos: usize, c: char);
    /// Removes character at the index.
    /// # Examples
    /// ```
    /// use non_ascii::NonAsciiMut;
    ///
    /// let mut string = String::from("élégant");
    /// // The following should panic
    /// // string.remove(1);
    /// assert_eq!(string.safe_remove(1), 'l');
    /// ```
    /// # Panics
    /// If the provided position is greater than the length (in graphemes) of the string, the function will panic.
    fn safe_remove(&mut self, pos: usize) -> char;
}

impl NonAscii for String {
    fn byte_position(&self, idx: usize) -> usize {
        assert!(self.graph_len() >= idx);
        match idx {
            0 => 0,
            tmp if tmp == self.graph_len() => self.len(),
            _ => UnicodeSegmentation::graphemes(self.as_str(), true)
                .take(idx)
                .map(|graph| graph.len())
                .sum(),
        }
    }

    fn graph_position(&self, byte_pos: usize) -> usize {
        assert!(self.is_char_boundary(byte_pos));
        match byte_pos {
            0 => 0,
            tmp if tmp == self.len() => self.graph_len(),
            _ => {
                let mut count = 0;
                UnicodeSegmentation::graphemes(self.as_str(), true)
                    .take_while(|gr| {
                        if count + gr.len() <= byte_pos {
                            count += gr.len();
                            true
                        } else {
                            false
                        }
                    })
                    .count()
            }
        }
    }

    // self.len() >= self.graph_len()
    fn graph_len(&self) -> usize {
        UnicodeSegmentation::graphemes(self.as_str(), true).count()
    }
}

impl NonAscii for str {
    fn byte_position(&self, idx: usize) -> usize {
        assert!(self.graph_len() >= idx);
        match idx {
            0 => 0,
            tmp if tmp == self.graph_len() => self.len(),
            _ => UnicodeSegmentation::graphemes(self, true)
                .take(idx)
                .map(|graph| graph.len())
                .sum(),
        }
    }

    fn graph_position(&self, byte_pos: usize) -> usize {
        assert!(self.is_char_boundary(byte_pos));
        match byte_pos {
            0 => 0,
            tmp if tmp == self.len() => self.graph_len(),
            _ => {
                let mut count = 0;
                UnicodeSegmentation::graphemes(self, true)
                    .take_while(|gr| {
                        if count + gr.len() <= byte_pos {
                            count += gr.len();
                            true
                        } else {
                            false
                        }
                    })
                    .count()
            }
        }
    }

    // self.len() >= self.graph_len()
    fn graph_len(&self) -> usize {
        UnicodeSegmentation::graphemes(self, true).count()
    }
}

impl NonAsciiMut for String {
    fn safe_insert(&mut self, idx: usize, c: char) {
        self.insert(self.byte_position(idx), c)
    }

    fn safe_remove(&mut self, idx: usize) -> char {
        self.remove(self.byte_position(idx))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const HELLO: &str = "Hello World!";
    const MULTIBYTE_FRENCH: &str = "L'élégance d'être déçu à Noël par une atmosphère délétère.";

    // Tests for one-byte characters.
    #[test]
    fn ascii_len() {
        let test_string = String::from(HELLO);
        assert!(test_string.len() == test_string.graph_len()); // 12
    }
    #[test]
    fn ascii_byte_position() {
        let test_string = String::from(HELLO);

        for i in 0..test_string.len() {
            assert!(test_string.byte_position(i) == i);
        }
    }

    #[test]
    fn ascii_graph_position() {
        let test_string = String::from(HELLO);

        for i in 0..test_string.len() {
            assert!(test_string.graph_position(i) == i);
        }
    }

    #[test]
    fn ascii_insert() {
        let test_string = String::from(HELLO);

        let mut normal_insert = test_string.clone();
        normal_insert.insert(11, 's');

        assert!(normal_insert.as_str() == "Hello Worlds!");

        let mut altered_insert = test_string.clone();
        altered_insert.safe_insert(11, 's');

        assert!(altered_insert.as_str() == "Hello Worlds!");
    }

    #[test]
    fn ascii_remove() {
        let test_string = String::from(HELLO);

        let mut normal_insert = test_string.clone();
        normal_insert.remove(0);

        assert!(normal_insert.as_str() == "ello World!");

        let mut altered_insert = test_string.clone();
        altered_insert.safe_remove(0);

        assert!(altered_insert.as_str() == "ello World!");
    }

    // Tests for multi-byte characters.
    #[test]
    fn multibyte_len() {
        let test_string = String::from(MULTIBYTE_FRENCH);
        assert!(test_string.len() > test_string.graph_len());
    }

    #[test]
    fn multibyte_byte_position() {
        let test_string = String::from(MULTIBYTE_FRENCH);

        let mut graph_indices = UnicodeSegmentation::grapheme_indices(MULTIBYTE_FRENCH, true);

        for i in 0..test_string.graph_len() {
            assert!(test_string.byte_position(i) == graph_indices.next().unwrap().0);
        }
    }

    #[test]
    fn multibyte_graph_position() {
        let test_string = String::from(MULTIBYTE_FRENCH);

        let graph_indices = UnicodeSegmentation::grapheme_indices(MULTIBYTE_FRENCH, true);

        let mut i = 0;
        for gr in graph_indices {
            assert!(test_string.graph_position(gr.0) == i);
            i += 1;
        }
    }

    #[test]
    fn multibyte_insert() {
        let test_string = String::from(MULTIBYTE_FRENCH);

        let mut graph_indices = UnicodeSegmentation::grapheme_indices(MULTIBYTE_FRENCH, true);

        let mut normal_insert = test_string.clone();
        let position = graph_indices.nth(11).unwrap().0;
        normal_insert.insert(position, 's');

        let mut altered_insert = test_string.clone();
        altered_insert.safe_insert(11, 's');

        assert!(altered_insert == normal_insert);
    }

    #[test]
    fn multibyte_remove() {
        let test_string = String::from(MULTIBYTE_FRENCH);

        let mut graph_indices = UnicodeSegmentation::grapheme_indices(MULTIBYTE_FRENCH, true);

        let mut normal_insert = test_string.clone();
        let position = graph_indices.nth(11).unwrap().0;
        normal_insert.remove(position);

        let mut altered_insert = test_string.clone();
        altered_insert.safe_remove(11);

        assert!(altered_insert == normal_insert);
    }
}
