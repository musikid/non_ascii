use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};

fn extern_byte_position(s: &str, idx: usize) -> usize {
    unicode_segmentation::UnicodeSegmentation::graphemes(s, true)
        .take(idx)
        .map(|graph| graph.len())
        .sum()
}

// The std version don't really use graphemes
fn std_byte_position(s: &str, idx: usize) -> usize {
    s.chars().take(idx).map(|graph| graph.len_utf8()).sum()
}

fn bench_byte_position(c: &mut Criterion) {
    let mut group = c.benchmark_group("byte_position");
    let string: &str = "L\'élégance d'être déçu à Noël par une atmosphère délétère.";

    group.bench_function(BenchmarkId::new("extern", "french_string"), |b| {
        b.iter(|| extern_byte_position(&string, 25))
    });
    group.bench_function(BenchmarkId::new("std", "french_string"), |b| {
        b.iter(|| std_byte_position(&string, 25))
    });

    group.finish();
}

fn extern_insert(s: &mut String, idx: usize) {
    s.insert(extern_byte_position(&s, idx), 't')
}

// The std version don't really use graphemes
fn std_insert(s: &mut String, idx: usize) {
    s.insert(std_byte_position(&s, idx), 't')
}

fn raw_insert(s: &mut String) {
    s.insert(31, 't')
}

fn bench_insert(c: &mut Criterion) {
    let mut group = c.benchmark_group("insert");
    let string: &str = "L\'élégance d'être déçu à Noël par une atmosphère délétère.";

    group.bench_function(BenchmarkId::new("extern", "french_string"), |b| {
        let mut string = String::from(string);
        b.iter(|| extern_insert(&mut string, 25))
    });
    group.bench_function(BenchmarkId::new("std", "french_string"), |b| {
        let mut string = String::from(string);
        b.iter(|| std_insert(&mut string, 25))
    });
    group.bench_function(BenchmarkId::new("raw", "french_string"), |b| {
        let mut string = String::from(string);
        b.iter(|| raw_insert(&mut string))
    });

    group.finish();
}

criterion_group!(benches, bench_byte_position, bench_insert);
criterion_main!(benches);
